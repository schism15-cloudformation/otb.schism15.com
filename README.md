Infrastructure for my OTB (Obligatory Twitch Bot). Written in CloudFormation (ew...).

Inputs:
- VPC

Resources:
- DNS
  - Route53 Zone
  - Route53 record
-Network
  - Subnets
  - Route Table association
- Compute
  - EC2 Nano
  - Key pair
  - Security Groups
  - Lambda to stop the EC2 after given time (or maybe use event bridge)
- Monitoring
  - TBD